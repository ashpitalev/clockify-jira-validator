
from clockify.clockify import Clockify as clockify_client
from requests import post
from jira import JIRA as jira_client
from datetime import datetime
from dateutil.tz import tzutc
from dateutil.parser import isoparse
from itertools import groupby
from re import search, match
from copy import deepcopy

class ClockifyJiraValidator:

    def __init__(self, jira_login, jira_token, clockify_token, clockify_user_query, jira_user_query, date_interval='', no_ignore_malformed_time_records=False, clockify_ws=''):
        self.jira_user_query = jira_user_query
        self.clockify_user_query = clockify_user_query
        self.jira_username = jira_login
        self.jira_token = jira_token
        self.jira_url = 'https://bitexch.atlassian.net'
        self.jira = jira_client(options={'server': self.jira_url}, basic_auth=(self.jira_username, self.jira_token))
        self.jira_issue_id_pattern = r'(?P<jira_issue_id>[A-Z]+\-\d+)'
        self.clockify_workspace = clockify_ws
        self.clockify_token = clockify_token
        self.clockify = clockify_client(self.clockify_token)
        self.clockify_duration_pattern = r'PT(?:(\d+)H)?(?:(\d+)M)?(?:(\d+)S)?'
        self.ignore_missing_jira_issues = True
        self.time_theshold = 1800
        self.no_ignore_malformed_time_records = no_ignore_malformed_time_records
        self.clockify_report_uri = 'https://api.clockify.me/api/workspaces/{wsid}/reports/summary/entries'
        self.clockify_report_payload = {'userGroupIds': [], 'userIds': [], 'projectIds': [], 'clientIds': [],
            'taskIds': [], 'tagIds': [], 'billable': 'BOTH', 'firstTime': False, 'archived': 'Active', 
            'startDate': '',
            'endDate': '',
            'me': 'TEAM', 'includeTimeEntries': True,
            'zoomLevel': 'week', 'name': '', 'groupingOn': False, 'groupedByDate': False, 'page': 0,
            'sortDetailedBy': 'timeAsc', 'count': 1000}
        if date_interval:
            d = date_interval.split(',')
            if len(d) != 2:
                raise RuntimeError('invalid date interval format')
            for _d in d:
                try:
                    isoparse(_d)
                except ValueError:
                    raise RuntimeError(f'invalid date format: {_d}')
            self.clockify_report_payload['startDate'] = d[0]
            self.clockify_report_payload['endDate'] = d[1]
        else:
            now = datetime.utcnow()
            yearbegin = datetime(now.year, 1, 1, 0, 0, 0, 0).isoformat() + 'Z'
            self.clockify_report_payload['startDate'] = yearbegin
            self.clockify_report_payload['endDate'] = datetime.utcnow().isoformat() + 'Z'
    
    def validate(self):
        user = next(filter(lambda x: x, self.jira.search_users(self.jira_user_query)), None)
        if user:
            print(f'found jira user: {user.displayName} ({user.emailAddress})')
            self.jira_user_id = user.accountId
        else:
            raise RuntimeError(f'jira: specified user not found, search query "{self.jira_user_query}"')
        issues = self.jira.search_issues(f'assignee = {self.jira_user_id}', maxResults=500, fields='timespent,status')
        c = self.clockify
        w = c.get_all_workspaces()
        if not self.clockify_workspace:
            if len(w) > 1:
                raise RuntimeError('clockify workspace is not specified and theres more than one')
            w = w[0]
        else:
            w = next(filter(lambda x: x['name'] == self.clockify_workspace, w), None)
            if not w:
                raise RuntimeError(f'workspace "{self.clockify_workspace}" was not found')
        wid = w['id']
        toks = self.clockify_user_query.split('=')
        if len(toks) != 2 and toks[0] not in ['email', 'name']:
            raise RecursionError(f'invalid clockify user query format {self.clockify_user_query}')
        u = next(filter(lambda x: toks[1].lower() in x[toks[0]].lower(), c.get_all_workspace_users(wid)), None)
        if not u:
            raise RuntimeError(f'clockify user {self.clockify_user_query} was not found')
        print(f'found clockify user: {u["name"]} ({u["email"]})')
        uid = u['id']
        payload = deepcopy(self.clockify_report_payload)
        payload['userIds'].append(uid)
        url = self.clockify_report_uri.format(wsid=wid)
        tes = post(url, json=payload, headers={'x-api-key': self.clockify_token})
        if tes.status_code != 200:
            raise RuntimeError('clockify api: could not get time entries', tes.text)

        if self.no_ignore_malformed_time_records:
            if not all(map(lambda x: bool(search(self.jira_issue_id_pattern, x['description'])), tes)):
                l = list(map(lambda x: x['description'], filter(lambda x: not bool(search(self.jira_issue_id_pattern, x['description'])), tes)))
                raise RuntimeError(f'clockify has malformed time entries {l}')
        ptes = {}

        for te in tes.json():
            if search(self.jira_issue_id_pattern, te['description']):
                issue_id = search(self.jira_issue_id_pattern, te['description']).group('jira_issue_id')
                if issue_id in ptes:
                    ptes[issue_id][te['id']] = te
                else:
                    ptes[issue_id] = {te['id']: te}
        messages = {}
        for k in ptes:
            messages[k] = []
            seconds = 0
            for te in ptes[k].values():
                d = list(map(lambda x: int(x) if x else 0, match(self.clockify_duration_pattern, te['timeInterval']['duration']).groups()))
                seconds += d[0] * 3600 + d[1] * 60 + d[2]
            issue = next(filter(lambda x: x.key == k, issues), None)
            if issue:
                # if issue is resolved or closed
                if issue.fields.status.id in ['5','6']:
                    # should have timespent and also it should be in accepted threshold
                    if not issue.fields.timespent:
                        messages[k].append({'severity':'error', 'type': f'timespent_not_set', 'details': f'clockify: {seconds_str(seconds)}'})
                    elif abs(int(issue.fields.timespent) - seconds) > self.time_theshold:
                        messages[k].append({
                            'severity':'error',
                            'type': 'time_threshold',
                            'details': f'clockify: {seconds_str(seconds)}, jira: {seconds_str(issue.fields.timespent)}'
                        })
            elif not self.ignore_missing_jira_issues:
                raise RuntimeError(f'issue {k} is not found in jira')
        return dict(list(filter(lambda x: x[1], messages.items())))

def seconds_str(s):
    h = s // 3600
    s = s % 3600
    m = s // 60
    s = s % 60
    return f'{h:02}:{m:02}:{s:02}'
