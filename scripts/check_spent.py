#!/usr/bin/env python

import clockify_jira_validator
import argparse
from pprint import pprint

def main():
    parser = argparse.ArgumentParser(description='Validate clockify reported time vs. jira. In order to map clockify reports to jira issues, clockify entries should contain jira issue ids (ex. JIRA-1234).')
    parser.add_argument('--jira-login', metavar='jira_login', type=str, required=True,
        help='jira login')
    parser.add_argument('--jira-token', metavar='jira_token', type=str, required=True,
        help='jira api token, you can generate it in https://id.atlassian.com/manage-profile/security/api-tokens')
    parser.add_argument('--clockify-token', metavar='clockify_token', type=str, required=True,
        help='clockify api token, you can generate it in https://clockify.me/user/settings')
    parser.add_argument('--clockify-user-query', metavar='clockify_user_query', type=str, required=True,
        help='clockify user query in the form: name|email=.*, example: name=john doe, searches are case insensitive')
    parser.add_argument('--jira-user-query', metavar='jira_user_query', type=str, required=True,
        help='jira user query in the form: name|email=.*, example: name=john doe, searches are case insensitive')
    parser.add_argument('--no-ignore-malformed-time-records', action='store_true',
        help='do not ignore malformed clockify time records (should contain jira id in description)')
    parser.add_argument('--clockify-workspace', metavar='clockify_workspace',
        help='specify clockify workspace, if not specified assume theres only one')
    parser.add_argument('--date-interval', metavar='date_interval',
        help='''specify date interval to pick clockify time entries from, format is <from_date>,<to_date>, where dates are in iso format: "YYYY-MM-DDTHH:MM:SS.XXXZ"''')
    
    args = parser.parse_args()
    c = clockify_jira_validator.ClockifyJiraValidator(args.jira_login, args.jira_token, args.clockify_token,
        clockify_user_query=args.clockify_user_query,
        jira_user_query=args.jira_user_query,
        date_interval=args.date_interval,
        no_ignore_malformed_time_records=args.no_ignore_malformed_time_records,
        clockify_ws=args.clockify_workspace)
    pprint(c.validate())

main()