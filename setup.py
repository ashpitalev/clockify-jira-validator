from setuptools import setup

setup(
    name="check_spent",
    version="0.5",
    scripts = [
        'scripts/check_spent.py',
        'scripts/check_spent',
        'scripts/check_spent.cmd'],
    packages = [ 'clockify_jira_validator' ],
    install_requires=['jira', 'clockify'],
    requires_python='>=3.5',
    author="Aleksei Shpitalev",
    author_email="aleksei.shpitalev@gmail.com",
    description="Validate clockify reported time vs. jira. In order to map clockify reports to jira issues, clockify entries shoul contain jira issue ids (ex. JIRA-1234)",
    keywords="clockify jira validate timespent",

)
